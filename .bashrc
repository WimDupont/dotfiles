#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

set -o vi

shopt -s histappend                      # append to history, don't overwrite it
export PROMPT_COMMAND="history -a; history -c; history -r; $PROMPT_COMMAND"

GPG_TTY=$(tty)
export GPG_TTY

export EDITOR=vim

# requires arc-gtk-theme package
export GTK_THEME=Arc-Dark

lf () {
    # `command` is needed in case `lfcd` is aliased to `lf`
    cd "$(command lf -print-last-dir "$@")"
}

# run single ssh-agent and cache keys for x duration
if ! pgrep -u "$USER" ssh-agent > /dev/null; then
    test ! -f "$XDG_RUNTIME_DIR/ssh-agent.env" && touch "$XDG_RUNTIME_DIR/ssh-agent.env" 
    ssh-agent -t 30m > "$XDG_RUNTIME_DIR/ssh-agent.env"
fi
if [[ ! -f "$SSH_AUTH_SOCK" ]]; then
    source "$XDG_RUNTIME_DIR/ssh-agent.env" >/dev/null
fi

function ts() {
	date -d @$1 "+%F %r"
}

function gitbig(){
	git rev-list --objects --all |
	  git cat-file --batch-check='%(objecttype) %(objectname) %(objectsize) %(rest)' |
	  sed -n 's/^blob //p' |
	  sort --numeric-sort --key=2 |
	  cut -c 1-12,41- |
	  $(command -v gnumfmt || echo numfmt) --field=2 --to=iec-i --suffix=B --padding=7 --round=nearest
}

function rssf(){
	url="$1"; curl -L -s "$url" | sfeed_web "$url"
}

function findstr(){
	grep -Rn . -e "$1"
}

function extract () {
    case $1 in
	*.tar.bz|*.tar.bz2|*.tbz|*.tbz2) tar xjvf "$1";;
	*.tar.gz|*.tgz) tar xzvf "$1";;
	*.tar.xz|*.txz) tar xJvf "$1";;
	*.tar) tar -xvf "$1";;
	*.zip) unzip "$1";;
	*.rar) unrar x "$1";;
	*.7z) 7z x "$1";;
	*.gz) gzip -dk "$1";;
	*.gpg) gpg --decrypt-files "$1";;
    esac
}

function vimis(){
	sudo vim $(whereis $1 | cut -d ' ' -f2)
}

function irc(){
	username=$1
	test -z "$username" && echo "Username?" && read username
	tmux new-session -d -s irc && tmux send-keys -t irc "ii -s irc.libera.chat -n $username" Enter
}

function nkill(){
	if [ -n "$1" ]; then
		for pid in `ps -ef | grep $1 | grep -v $$ | grep -v "grep $1" | tr -s ' ' | cut -f2 -d' '`; do
			echo "killing pid: $pid"
			kill -9 "$pid"
		done
	fi
}

function leak(){
	name="$1"
	[[ ! -f "$name" ]] && echo "$1 not found in current directory" >&2 && return 1; 
	base="${name%.*}"
	case "${name##*.}" in
		"c")
			gcc -g "$name" -o "$base"
			;;
		"cpp")
			g++ -g "$name" -o "$base"
			;;
		*)
			base="$name"
			;;
	esac
	valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes -s "$base"
}

function cex() {
	txt=$(mktemp) || exit 1
	command cex -f "$txt" && cd "$(cat "$txt")"
	rm "$txt"
}

alias ls='ls --color=auto'
alias ll='ls -l'
alias weather='curl wttr.in'
alias bt='bluetoothctl'
alias e='cex'
alias wifi='nmcli radio wifi'
alias todo='vim ~/authored/.todo.adoc'
alias signal-cli='sh ~/Downloads/signal-cli/signal-cli.sh'
alias lg='lazygit'
alias rss='export SFEED_URL_FILE="$HOME/.sfeed/urls" && [ -f "$SFEED_URL_FILE" ] || touch "$SFEED_URL_FILE" && sfeed_curses ~/.sfeed/feeds/*'
alias rssu='sfeed_update ~/.sfeed/sfeedrc'
alias rsse='vim ~/.sfeed/sfeedrc'
alias lockcheck='faillock --user $USER'
alias lockfree='faillock --user $USER --reset'

PS1='[\u@\h \W]\$ '
