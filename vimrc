" All system-wide defaults are set in $VIMRUNTIME/archlinux.vim (usually just
" /usr/share/vim/vimfiles/archlinux.vim) and sourced by the call to :runtime
" you can find below.  If you wish to change any of those settings, you should
" do it in this file (/etc/vimrc), since archlinux.vim will be overwritten
" everytime an upgrade of the vim packages is performed.  It is recommended to
" make changes after sourcing archlinux.vim since it alters the value of the
" 'compatible' option.

" This line should not be removed as it ensures that various options are
" properly set to work with the Vim-related packages.
runtime! archlinux.vim

" If you prefer the old-style vim functionalty, add 'runtime! vimrc_example.vim'
" Or better yet, read /usr/share/vim/vim80/vimrc_example.vim or the vim manual
" and configure vim to your own liking!

" do not load defaults if ~/.vimrc is missing
"let skip_defaults_vim=1

"set spell
set tw=0
set wrapmargin=1
set nu relativenumber
set hlsearch
set ic

let t:is_transparent = 1
let t:is_book = 0
let g:netrw_winsize = 20

function! Toggle_transparent()
	:colors default
	call Set_Highlights()
	if t:is_transparent == 0
		hi Normal guibg=NONE ctermbg=NONE
		let t:is_transparent = 1
	else
		hi Normal guibg=#111111 ctermbg=black
		let t:is_transparent = 0
	endif
endfunction

function! Set_Highlights()
	highlight VertSplit		cterm=none	gui=none
	highlight StatusLineNC		ctermbg=241	ctermfg=0
	highlight StatusLine		ctermbg=0	ctermfg=238
	highlight IncSearch		ctermbg=3	ctermfg=1
	highlight Search		ctermbg=241	ctermfg=3
	highlight StatusLineTerm	ctermbg=238	ctermfg=0	cterm=NONE
	highlight StatusLineTermNC	ctermbg=0	ctermfg=238	cterm=NONE
endfunction

function! Toggle_bookmode()
	if t:is_book == 0
		set tw=80
		set wrapmargin=0
		call Format()
		let t:is_book = 1
	else
		set tw=0
		set wrapmargin=1
		call Format()
		let t:is_book = 0
	endif
endfunction

function! Change_Res(op,val,isvert)
	let c = v:count == 0 ? 1*a:val : v:count*a:val
	let cmd = a:isvert ? ':vert res' : ':res'
	exec cmd . a:op . c
	echo a:op . c
endfunction

function! Set_IDE()
	:Lex
	:bo term
	:resize -10
	wincmd w
	wincmd w
endfunction

function! Format()
	call feedkeys("gggqG")
	call Clear_whitespace()
endfunction

function! Clear_whitespace()
	:%s/\ *\ /\ /g
endfunction

runtime! ftplugin/man.vim

tnoremap <leader>w <C-w><C-w>
tnoremap <leader>i <C-w><S-n>
tnoremap <leader>R <C-w><S-n>:so /etc/vimrc<CR>i
tnoremap <leader>t <C-w><S-n>: call Toggle_transparent()<CR>i
tnoremap <leader><up> <C-w><S-n>:<C-U>call Change_Res('+',5,0)<CR>i
tnoremap <leader><down> <C-w><S-n>:<C-U>call Change_Res('-',5,0)<CR>i
tnoremap <leader><right> <C-w><S-n>:<C-U>call Change_Res('+',5,1)<CR>i
tnoremap <leader><left> <C-w><S-n>:<C-U>call Change_Res('-',5,1)<CR>i

nnoremap <leader><up> :<C-U>call Change_Res('+',5,0)<CR>
nnoremap <leader><down> :<C-U>call Change_Res('-',5,0)<CR>
nnoremap <leader><right> :<C-U>call Change_Res('+',5,1)<CR>
nnoremap <leader><left> :<C-U>call Change_Res('-',5,1)<CR>
nnoremap <leader>w <C-w><C-w>
nnoremap <leader>R :so /etc/vimrc<CR>
nnoremap <leader>T :bo ter<CR>
nnoremap <leader>t : call Toggle_transparent()<CR>
nnoremap <leader>b : call Toggle_bookmode()<CR>
nnoremap <leader>f : call Format()<CR>
nnoremap <leader>c : call Clear_whitespace()<CR>
nnoremap <leader>e : call Set_IDE()<CR>

vnoremap <leader>y "+y
vnoremap <leader>p "+p
vnoremap <leader>P "_dP

cmap suw w !sudo tee > /dev/null %

autocmd BufWritePost,FileWritePost config.def.h !sudo rm config.h && sudo make install
autocmd VimEnter * call Set_Highlights()

autocmd FileType asciidoc map <F2> :w <CR> :!htmlconv %<CR>
autocmd FileType markdown map <F2> :w <CR> :!htmlconv %<CR>
autocmd FileType c map <F2> :w <CR> :!gcc % -o %< && ./%< <CR>
autocmd FileType c map <F3> :w <CR> :!gcc % -g -o %< && gdb ./%< <CR>
autocmd FileType cpp map <F2> :w <CR> :!g++ % -o %< && ./%< <CR>
autocmd FileType cpp map <F3> :w <CR> :!g++ % -g -o %< && gdb ./%< <CR>
autocmd FileType c,cpp map <F4> :w <CR> :!make && ./%< <CR>
autocmd FileType c,cpp map <F5> :w <CR> :!make && gdb ./%< <CR>

autocmd FileType c :command! -bar -nargs=* Run :w | :!gcc % -o %< && ./%< <args>
autocmd FileType c :command! -bar -nargs=* Rund :w | :!gcc % -o %< && gdb --args ./%< <args>
autocmd FileType cpp :command! -bar -nargs=* Run :w | :!g++ % -o %< && ./%< <args>
autocmd FileType cpp :command! -bar -nargs=* Rund :w | :!g++ % -o %< && gdb --args ./%< <args>
autocmd FileType c,cpp :command! -bar -nargs=* Runm :w | :!make && ./%< <args>
autocmd FileType c,cpp :command! -bar -nargs=* Runmd :w | :!make && gdb --args ./%< <args>
